//
//  ViewController.swift
//  AR Dice
//
//  Created by iulian david on 8/13/17.
//  Copyright © 2017 iulian david. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController {

    @IBOutlet var sceneView: ARSCNView!
    
    
    
    var diceArray = [SCNNode]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        
        //add light
        sceneView.autoenablesDefaultLighting = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        print("Session is supported : \(ARConfiguration.isSupported)")
        print("World Tracking is supported : \(ARWorldTrackingConfiguration.isSupported)")
        
        
        // detecting horizontal plane
        configuration.planeDetection = .horizontal
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    //MARK: - Dice Rendering Methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let touchLocation = touch.location(in: sceneView)
            
            //convert location from 2D to 3D
            let results = sceneView.hitTest(touchLocation, types: .existingPlaneUsingExtent)
            if let hitResult = results.first {
                addDice(atLocation: hitResult)
            }
            
        }
    }
    
    private func addDice(atLocation location: ARHitTestResult) {
        // Create a new scene
        let diceScene = SCNScene(named: "art.scnassets/diceCollada.scn")!
        
        guard let diceNode = diceScene.rootNode.childNode(withName: "Dice", recursively: true) else {
            return
        }
        
        //adding diceNode.boundingSphere.radius to place the object above the plane
        let yPosition: Float = location.worldTransform.columns.3.y + diceNode.boundingSphere.radius
        
        diceNode.position = SCNVector3(
            x: location.worldTransform.columns.3.x,
            y: yPosition,
            z: location.worldTransform.columns.3.z)
        
        diceArray.append(diceNode)
        // Set the scene to the view
        sceneView.scene.rootNode.addChildNode(diceNode)
        roll(dice: diceNode)
    }
    
    private func roll(dice: SCNNode) {
        let randomX = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
        
        let randomZ = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
        
        dice.runAction(SCNAction.rotateBy(
            x: CGFloat(randomX * 10),
            y: 0,
            z: CGFloat(randomZ * 5),
            duration: 0.5))
    }
    
    func rollAll() {
        if !diceArray.isEmpty {
            diceArray.forEach({ (dice) in
                roll(dice: dice)
            })
            
        }
    }

    
    //MARK: - Actions
    @IBAction func rollAgain(_ sender: Any) {
        rollAll()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        rollAll()
    }
    @IBAction func removeAllDices(_ sender: Any) {
        if !diceArray.isEmpty {
            diceArray.forEach({ (dice) in
                dice.removeFromParentNode()
            })
        }
    }
}

//MARK: - ARSCNViewDelegate methods
extension ViewController: ARSCNViewDelegate {
    
    // Detected a horizontal plane and set up an height and width
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        guard let planeAnchor = anchor as? ARPlaneAnchor else {
            return
        }
        
        
        let planeNode = createPlane(withPlaneAnchor: planeAnchor)
        
        node.addChildNode(planeNode)
        
    }
    
    //MARK: - Plane Rendering Merhods
    func createPlane(withPlaneAnchor planeAnchor: ARPlaneAnchor) -> SCNNode {
        let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
        
        let planeNode = SCNNode()
        
        planeNode.position = SCNVector3(x: planeAnchor.center.x, y: 0, z: planeAnchor.center.z)
        
        
        //transforming the node to 2d matrix
        //rotating to 90°
        planeNode.transform = SCNMatrix4MakeRotation(-Float.pi/2, 1, 0, 0)
        
        
        //applying a material with the use of a template png file. PNG is used because of transparency
        let gridMaterial = SCNMaterial()
        gridMaterial.diffuse.contents = #imageLiteral(resourceName: "grid")
        
        plane.materials = [gridMaterial]
        
        planeNode.geometry = plane
        
        return planeNode
    }
}
